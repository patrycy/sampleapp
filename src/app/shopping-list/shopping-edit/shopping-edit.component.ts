import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Ingridient} from '../../shared/ingridient.model';
import {ShoppingListService} from './service/shopping-list.service';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @ViewChild('ingridientName') nameInputRef: ElementRef;
  @ViewChild('ingridientAmount') amountInputRef: ElementRef;

  constructor(private slService: ShoppingListService) { }

  ngOnInit(): void {
  }

  onAddItem() {
    this.slService.addIngridient(
      new Ingridient(
        this.nameInputRef.nativeElement.value,
        this.amountInputRef.nativeElement.value)
    );
  }

}

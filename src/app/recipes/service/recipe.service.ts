import {Recipe} from '../recipe.model';
import {EventEmitter, Injectable} from '@angular/core';
import {Ingridient} from '../../shared/ingridient.model';
import {ShoppingListService} from '../../shopping-list/shopping-edit/service/shopping-list.service';

@Injectable()
export class RecipeService {

  recipeSelected = new EventEmitter<Recipe>();

  private recipes: Recipe[] = [
    new Recipe(
      'A Test Onion Recipe',
      'Test decription',
      'https://cdn.pixabay.com/photo/2018/01/21/17/17/onions-3097053_960_720.jpg',
      [
        new Ingridient('Sweet onion', 123),
        new Ingridient('Hot onion', 666)
      ]),
    new Recipe(
      'A Test Onion Recipe2',
      'Test decription2',
      'https://images-gmi-pmc.edge-generalmills.com/2703b6a2-ce28-4d1b-9119-5f7aee272c1f.jpg',
      [
        new Ingridient('Sweet onion2', 123),
        new Ingridient('Hot onion2', 666)
      ])
  ];

  constructor(private slService: ShoppingListService) {
  }

  getRecipies() {
    return this.recipes.slice();
  }

  getByIndex(index: number) {
    return this.recipes[index];
  }

  addToShoppingList(ingridientsToAdd: Ingridient[]) {
    this.slService.addIngridients(ingridientsToAdd);
  }

}

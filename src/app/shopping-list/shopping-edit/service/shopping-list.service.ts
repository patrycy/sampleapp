import {Ingridient} from '../../../shared/ingridient.model';
import {EventEmitter} from '@angular/core';

export class ShoppingListService {
  private ingridients: Ingridient[] = [
    new Ingridient('Onion', 123123),
    new Ingridient('Garlic', 321321)
  ];

  ingridientChanged = new EventEmitter<Ingridient[]>();

  get getIngridients(): Ingridient[] {
    return this.ingridients.slice();
  }

  addIngridient(ingridient: Ingridient) {
    this.ingridients.push(ingridient);
    this.ingridientChanged.emit(this.ingridients.slice());
  }

  addIngridients(ingridients: Ingridient[]) {
    this.ingridients.push(...ingridients);
    this.ingridientChanged.emit(this.ingridients.slice());
  }

}

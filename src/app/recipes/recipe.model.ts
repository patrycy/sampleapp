import {Ingridient} from '../shared/ingridient.model';

export class Recipe {
  public name: string;
  public desription: string;
  public imapePath: string;
  public ingridients: Ingridient[];

  constructor(name: string, desc: string, image: string, ingridients: Ingridient[]) {
    this.name = name;
    this.desription = desc;
    this.imapePath = image;
    this.ingridients = ingridients;
  }

}
